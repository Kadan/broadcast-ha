package com.ilabservice.darwinrecorder.hlslive;


import com.ilabservice.darwinrecorder.hlslive.ffmpeg.PushUtils;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//import javax.persistence.Basic;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootApplication
@Configuration
@ComponentScan
@EnableScheduling
@EnableSwagger2
@EnableEurekaClient
public class HlsliveApplication {


	@Value("${ffmpegpath}")
	private String ffmpegpath;


	@Value("${ffprobepath}")
	private String ffprobepath;

	@Bean
	public FFmpeg ffmpeg(){

		FFmpeg ffmpeg  = null;
		try {
			ffmpeg = new FFmpeg(ffmpegpath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ffmpeg;
	}

	@Bean
	FFprobe ffprobe() {
		FFprobe ffprobe = null;

		try {
			ffprobe = new FFprobe(ffprobepath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ffprobe;
	}


	@Bean
	public RestTemplate restTemplate(){ return new RestTemplate();}


	@Bean
	@LoadBalanced
	public RestTemplate lbRestTemplate(){ return new RestTemplate();}

	@Bean
	public RecorderRedisHandler recorderRedisHandler(){
		return new RecorderRedisHandler();
	}


	@Bean
	InfluxDBResultMapper resultMapper(){
		return new InfluxDBResultMapper();
	}


	@Bean
	public PushUtils pushUtils(){return  new PushUtils();}

	@Bean
	public Handler handler(){return  new Handler();}

	public static void main(String[] args) {
		SpringApplication.run(HlsliveApplication.class, args);

	}




}
