package com.ilabservice.darwinrecorder.hlslive.cron;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.elasticjob.lite.annotation.ElasticSimpleJob;
import com.ilabservice.darwinrecorder.hlslive.controller.RTMPPusher;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.PushUtils;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ElasticSimpleJob(cron = "0/3 * * * * ? ", jobName = "brocastDaemon",shardingTotalCount = 1, jobParameter = "brocastDaemon",shardingItemParameters = "0=A,1=B,2=C,3=D")
@Component
public class BroadcastCorn implements SimpleJob {
    @Autowired
    RecorderRedisHandler recorderRedisHandler;

    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    PushUtils pushUtils;

    @Autowired
    Handler handler;

    @Autowired
    RestTemplate lbRestTemplate;
    private static final Logger log = LoggerFactory.getLogger(BroadcastCorn.class);

    @Override
    public void execute(ShardingContext shardingContext) {

        Map<String, String> broadcastMap = recorderRedisHandler.getRTMPBrocastMap();
        log.info("push RTMP Corn started ...");
        List<String> fetchList = new ArrayList<String>();
        List<String> instanceInfos = discoveryClient.getServices();
        if (instanceInfos != null && instanceInfos.size() > 0) {
            instanceInfos.stream().forEach(s -> {
                if (StringUtils.startsWithIgnoreCase(s, "darwin-service-")) {
                    String pusherString = lbRestTemplate.getForObject("http://" + s + "/api/v1/pushers", String.class);
                    JSONObject jsonObject = JSONObject.parseObject(pusherString);
                    int total = jsonObject.getIntValue("total");
                    if (total > 0) {
                        JSONArray jSONArray = jsonObject.getJSONArray("rows");
                        jSONArray.stream().map(jo -> {
                            JSONObject jo1 = (JSONObject) jo;
                            return jo1;
                        }).forEach(s1 -> {
                            fetchList.add(s1.getString("source"));
                        });
                    }
                }
            });
        }
        fetchList.stream().filter(s -> !handler.rtmpInProgress(org.apache.commons.lang3.StringUtils.substringAfterLast(s, "/"))
                && broadcastMap.keySet().contains(org.apache.commons.lang3.StringUtils.substringAfterLast(s, "/"))).forEach(
                s -> {
                    log.info("push  " + s + " to RTMP Server ");
                    String stream = org.apache.commons.lang3.StringUtils.substringAfterLast(s, "/");
                    recorderRedisHandler.saveRTMPBroadcastStatus(stream,stream);
                    pushUtils.push(s, stream);
                }
        );
    }
}
