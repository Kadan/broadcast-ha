package com.ilabservice.darwinrecorder.hlslive.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.PushUtils;
import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
import com.ilabservice.darwinrecorder.hlslive.measurements.RtmpMeasurement;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import org.apache.commons.lang.StringUtils;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class RTMPPusher {
    private static final Logger log = LoggerFactory.getLogger(RTMPPusher.class);
    @Autowired
    PushUtils pushUtils;

    @Autowired
    Handler handler;

    @Autowired
    InfluxDbUtils influxDbUtils;

    @Autowired
    RecorderRedisHandler recorderRedisHandler;

    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    RestTemplate lbRestTemplate;


    @GetMapping("/rtmpStart/{stream}")
    public ResponseEntity<String> startRTMP(HttpServletRequest request, @PathVariable String stream){

        Map<String,String> broadcastMap = recorderRedisHandler.getRTMPBrocastMap();

        if(broadcastMap.keySet().contains(stream)){
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("rtmp");
            em.setReason("stream already on live");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
            return new ResponseEntity<String>("The stream " + stream + " is already on live ...", HttpStatus.BAD_REQUEST);
        }

        List<String> fetchList = getAllRTSP();

        if(!fetchList.stream().anyMatch(s->StringUtils.endsWithIgnoreCase(s.trim(),stream))){
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("rtmp");
            em.setReason("stream not alive on darwin");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
            return new ResponseEntity<String>("The stream " + stream + " dose not exists on darwin ...", HttpStatus.BAD_REQUEST);
        }

        fetchList.stream().filter(s->StringUtils.endsWithIgnoreCase(s.trim(),stream)).limit(1).forEach(s->{
            pushUtils.push(s,stream);
            recorderRedisHandler.saveRTMPBroadcastStatus(stream,stream);
            RtmpMeasurement rm = new RtmpMeasurement();
            rm.setName(stream);
            rm.setTime(System.currentTimeMillis());
            rm.setTotalBroadcasting(broadcastMap.keySet().size()+1);
            rm.setType("rtmp");
            Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
            influxDbUtils.influxDB.write(point);
        });
        return new ResponseEntity<String>("Started to push " + stream, HttpStatus.OK);
    }

    @GetMapping("/rtmpStop/{stream}")
    public ResponseEntity<String> stopRTMP(HttpServletRequest request, @PathVariable String stream){
        Map<String,String> broadcastMap = recorderRedisHandler.getRTMPBrocastMap();
        if(!broadcastMap.keySet().contains(stream)){
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("rtmp");
            em.setReason("stream already been stopped");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
            return new ResponseEntity<String>("The stream " + stream + " is already been stopped ...", HttpStatus.BAD_REQUEST);
        }

        List<String> fetchList = getAllRTSP();

        handler.stopRTMP(stream);
        recorderRedisHandler.removeRTMPBroadcastStatus(stream);

        if(fetchList.stream().anyMatch(s->StringUtils.containsIgnoreCase(s,stream))){
//            handler.stopRTMP(stream);
//            recorderRedisHandler.removeRTMPBroadcastStatus(stream);
            return new ResponseEntity<String>("The  " + stream + " rtmp has been stopped", HttpStatus.OK);
        }
        else
            return new ResponseEntity<String>("The stream " + stream + " dose not exists on darwin ... but stop from further broadcasting", HttpStatus.OK);
    }


    private List<String> getAllRTSP(){
        List<String> fetchList = new ArrayList<String>();
        List<String> instanceInfos = discoveryClient.getServices();
        if (instanceInfos != null && instanceInfos.size() > 0) {
            instanceInfos.stream().forEach(s -> {
                if (StringUtils.startsWithIgnoreCase(s, "darwin-service-")) {
                    String pusherString = lbRestTemplate.getForObject("http://" + s + "/api/v1/pushers", String.class);
                    JSONObject jsonObject = JSONObject.parseObject(pusherString);
                    int total = jsonObject.getIntValue("total");
                    if (total > 0) {
                        JSONArray jSONArray = jsonObject.getJSONArray("rows");
                        jSONArray.stream().map(jo -> {
                            JSONObject jo1 = (JSONObject) jo;
                            return jo1;
                        }).forEach(s1 -> {
//                            log.info(">>>> found " + s1.getString("source"));
                            fetchList.add(s1.getString("source"));
                        });
                    }
                }
            });
        }
        return fetchList;
    }
}
