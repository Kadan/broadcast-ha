package com.ilabservice.darwinrecorder.hlslive.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;

public class RecorderRedisHandler {

    private final String RECORDER_MAP = "RECORDER_MAP";

    private final String RECORDER_STOPPING_MAP = "RECORDER_STOPPING_MAP";

    private final String RTMP_RECORDER_MAP = "RTMP_RECORDER_MAP";

    private final String RTMP_BROADCAST_MAP = "RTMP_BROADCAST_MAP";

    @Autowired
    RedisTemplate<String, Object> redisTemplate;


    public Map<String, String> getRTMPBrocastMap(){
        Map<String, String> returnMap =  redisTemplate.<String,String>opsForHash().entries(RTMP_BROADCAST_MAP);
        if(returnMap == null )returnMap=new HashMap<String, String>();
        return returnMap;
    }

    public void saveRTMPBroadcastStatus(String stream,String rtmpUrl){
        this.redisTemplate.opsForHash().put(RTMP_BROADCAST_MAP,stream,rtmpUrl);
    }

    public void removeRTMPBroadcastStatus(String stream){
        this.redisTemplate.opsForHash().delete(RTMP_BROADCAST_MAP,stream);
    }
    
    public Map<String, String> getRTMPRecorderMap(){
        Map<String, String> returnMap =  redisTemplate.<String,String>opsForHash().entries(RTMP_RECORDER_MAP);
        if(returnMap == null )returnMap=new HashMap<String, String>();
        return returnMap;
    }

    public void saveRTMPRecorderStatus(String stream,String rtmpUrl){
        this.redisTemplate.opsForHash().put(RTMP_RECORDER_MAP,stream,rtmpUrl);
    }

    public void removeRTMPRecorderStatus(String stream){
        this.redisTemplate.opsForHash().delete(RTMP_RECORDER_MAP,stream);
    }

    public Map<String, Long> getRecorderMap(){
        Map<String, Long> returnMap = redisTemplate.<String, Long>opsForHash().entries(RECORDER_MAP);
        if(returnMap ==null) returnMap = new HashMap<String, Long>();
        return returnMap;
    }

    public void saveRecorderStatus(String stream,Long timeStamp){
        this.redisTemplate.opsForHash().put(RECORDER_MAP,stream,timeStamp);
    }

    public void removeRecorderStatus(String stream){
        this.redisTemplate.opsForHash().delete(RECORDER_MAP,stream);
    }

    public Map<String, String> getStoppingMap(){
        Map<String, String> returnMap =  redisTemplate.<String,String>opsForHash().entries(RECORDER_STOPPING_MAP);
        if(returnMap == null )returnMap=new HashMap<String, String>();
        return returnMap;
    }

    public void saveStoppingStatus(String stream){
        this.redisTemplate.opsForHash().put(RECORDER_STOPPING_MAP,stream,stream);
    }

    public void removeStoppingStatus(String stream){
        this.redisTemplate.opsForHash().delete(RECORDER_STOPPING_MAP,stream);
    }


}
