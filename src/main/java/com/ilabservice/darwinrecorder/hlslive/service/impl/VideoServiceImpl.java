//package com.ilabservice.darwinrecorder.hlslive.service.impl;
//
//import com.ilabservice.darwinrecorder.hlslive.entity.Video;
//import com.ilabservice.darwinrecorder.hlslive.repository.VideoRepository;
//import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@Service
//public class VideoServiceImpl implements VideoService {
//    @Autowired
//    VideoRepository videoRepository;
//
//    @Override
//    public List<Video> findByVideoName(String videoName) {
//        return videoRepository.findByVideo(videoName);
//    }
//
//    @Override
//        public void save(Video video) {
//        videoRepository.save(video);
//        }
//
//    @Override
//    public Map<String,Video> findByVideoNameIn(List<String> videoNames) {
//        Map<String,Video> retMap = new HashMap<String,Video>();
//
//        List<Video> videos = videoRepository.findByVideoIn(videoNames);
//        if(videos!=null){
//            videos.stream().forEach(s->retMap.put(s.getVideo(),s));
//        }
//        return retMap;
//    }
//
//    @Override
//    public List<Video> findByStartTimeAndEndTime(long startTime, long endTime) {
//        List<Video> videoList = videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThan(endTime,startTime);
//        return videoList;
//    }
//
//    @Override
//    public List<Video> findByStartTimeAndEndTimeAndCameraId(long startTime, long endTime,String cameraId) {
//        List<Video> videoList = videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraId(endTime,startTime,cameraId);
//        return videoList;
//    }
//
//}
