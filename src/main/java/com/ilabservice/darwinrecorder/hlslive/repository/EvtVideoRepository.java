//package com.ilabservice.darwinrecorder.hlslive.repository;
//
//import com.ilabservice.darwinrecorder.hlslive.entity.EvtVideo;
//import com.ilabservice.darwinrecorder.hlslive.entity.Video;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.List;
//
//public interface EvtVideoRepository extends JpaRepository<EvtVideo,Long> {
//    List<EvtVideo> findByVideo(String video);
//    List<EvtVideo> findByVideoIn(List<String> videos);
//    List<EvtVideo> findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraId(long endTime, long startTime, String cameraId);
//    List<EvtVideo> findByStartTimeLessThanEqualAndEndTimeGreaterThan(long endTime, long startTime);
//}
