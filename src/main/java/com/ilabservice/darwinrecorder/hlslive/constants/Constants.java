package com.ilabservice.darwinrecorder.hlslive.constants;

public class Constants {

    public static String firstFrameImgName = "firstFrame.jpg";
    public static  String tailorVideoPrefix = "-ted";
    public static  String tailorIndexFile = "index.txt";

    public static String event_MEASUREMENT = "EVENT_MEASUREMENTS";
    public static String rtmp_MEASUREMENT = "RTMP_MEASUREMENTS";
    public static String darwin_EVENT_MEASUREMENT = "DARWIN_EVENT_MEASUREMENTS";
    public static String error_MEASUREMENT = "ERROR_MEASUREMENTS";
    public static String recorder_MEASUREMENT = "RECORDER_MEASUREMENTS";

    public static String darwinPusherAPISuffix = "/api/v1/pushers";
}
